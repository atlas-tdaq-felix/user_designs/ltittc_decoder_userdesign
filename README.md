# ltittc_decoder_userdesign

## Introduction

FELIX distributes the TTC information in the LTI-TTC format in the FULLMODE(phase2) and INTERLAKEN flavour. 
For the specification, refer to the [FELIX Phase 2 firmware specification](https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/docs/FELIX_Phase2_firmware_specs.pdf).

This example design does the required 8b10b decoding in FPGA fabric (VHDL) so it can be easily ported to 
devices which don't support 8b10b at 9618.96 Mb/s, such as the Cyclone 10GX FPGA.

## Hog project

The project includes Hog to create an initial Vivado project.

```
#Initialize the git repository submodules, to get Hog:
git submodule update --init
#Create the Vivado project.
./Hog/CreateProject.sh ltittc_decoder_userdesign
```

## VUnit simulation
Make sure that a simulator (questasim or modelsim) and Vivado are in the PATH.

```
cd vunit
./run.py --gui
```

## Structure

The TTC transmitter which is normally included in FELIX is instantiated in the testbench, as well as a TTC pattern generator and a checker of the decoded TTC signal.

The user design is intended for simulation only, but can be easily ported to several devices and transceiver types. 
It contains the following components:
 * gtwizard_0: A simulation model of a 7 series Xilinx GTH transceiver with 9618.96 Mb/s 8b10b TX (32+4b) and 9618.96 Mb/s raw RX (40b)
 * dec_8b10b_4x: 4 parallel 8b10b decoders with K28.5 alignment
 * ltittc_wrapper: Decodes the 8b10b decoded signal into a 40 MHz BC clock and the decoded TTC_data_type record.


![Structure of the testbench and user design](Doc/ltittc_decoder_userdesign.png "Structure of the testbench and user design")

## Data type

The decoded `TTC_out_s` signal of the type `TTC_data_type`, contained in `lti_types_package.vhd` contains the following fields:

```
    type TTC_data_type is record
        --***Phase2***
        -- Header
        MT                  : std_logic;
        PT                  : std_logic;
        Partition           : std_logic_vector(1 downto 0);
        BCID                : std_logic_vector(11 downto 0);
        SyncUserData        : std_logic_vector(15 downto 0);
        SyncGlobalData      : std_logic_vector(15 downto 0);
        TS                  : std_logic;
        ErrorFlags          : std_logic_vector(3 downto 0);

        -- TTC Message
        SL0ID               : std_logic;
        SOrb                : std_logic;
        Sync                : std_logic;
        GRst                : std_logic;
        L0A                 : std_logic;
        L0ID                : std_logic_vector(37 downto 0);
        OrbitID             : std_logic_vector(31 downto 0);
        TriggerType         : std_logic_vector(15 downto 0);
        LBID                : std_logic_vector(15 downto 0);

        --User message
        AsyncUserData       : std_logic_vector(63 downto 0);

        LTI_decoder_aligned   : std_logic;
        LTI_CRC_valid         : std_logic;


    end record;
```
