#!/usr/bin/python3
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file,
# You can obtain one at http://mozilla.org/MPL/2.0/.
#
# Copyright (c) 2014-2022, Lars Asplund lars.anders.asplund@gmail.com

"""
Vivado IP
---------
Demonstrates compiling and performing behavioral simulation of
Vivado IPs with VUnit.
"""

from pathlib import Path
from vunit import VUnit
from vivado_util import add_vivado_ip

import os

ROOT = Path(__file__).parent / ".."
SRC_PATH = ROOT / "src"

VU = VUnit.from_argv()
#VU.set_sim_option('modelsim.vsim_flags', ['-suppress 8684'])
XILINX_VIVADO = os.getenv('XILINX_VIVADO')


# Create library 'lib'
lib = VU.add_library("lib")

lib.add_source_files(ROOT / "Top/ltittc_decoder_userdesign/source/decoder_8b10b/dec_8b10b_4x.vhd")
lib.add_source_files(ROOT / "Top/ltittc_decoder_userdesign/source/decoder_8b10b/dec_8b10b.vhd")
lib.add_source_files(ROOT / "Top/ltittc_decoder_userdesign/source/packages/lti_types_package.vhd")
lib.add_source_files(ROOT / "Top/ltittc_decoder_userdesign/source/userdesign/top_ltittc_decoder_userdesign.vhd")
lib.add_source_files(ROOT / "Top/ltittc_decoder_userdesign/source/decoder_ltittc/ltittc_wrapper.vhd")
lib.add_source_files(ROOT / "Top/ltittc_decoder_userdesign/source/decoder_ltittc/ltittc_decoder.vhd")
lib.add_source_files(ROOT / "Top/ltittc_decoder_userdesign/source/encoder_ltittc/TTC_LTI_Transmitter.vhd")
lib.add_source_files(ROOT / "Top/ltittc_decoder_userdesign/source/encoder_ltittc/CRC16_LTI.vhd")
lib.add_source_files(ROOT / "Top/ltittc_decoder_userdesign/simulation/ltittc_decoder_userdesign_tb.vhd")
lib.add_source_files(ROOT / "Top/ltittc_decoder_userdesign/simulation/gtwizard_0_sim_netlist.vhdl")
lib.add_source_files(XILINX_VIVADO + "/data/verilog/src/glbl.v")

add_vivado_ip(
    VU,
    output_path=ROOT / "vivado_libs",
    project_file=ROOT / "Projects/ltittc_decoder_userdesign/ltittc_decoder_userdesign.xpr",
)
VU.set_sim_option("modelsim.vsim_flags", ['-voptargs=+acc', 'lib.glbl'])

VU.main()
