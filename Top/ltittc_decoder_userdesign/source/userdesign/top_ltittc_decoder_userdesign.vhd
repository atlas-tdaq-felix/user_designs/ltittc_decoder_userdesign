--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;
use ieee.numeric_std_unsigned.all;
use work.lti_types_package.all;

library UNISIM;
use UNISIM.VComponents.all;

entity ltittc_decoder_userdesign is
    Port ( 
        reset_i : in std_logic;
        clk60_i : in std_logic;
        clk240_i : in std_logic;
        txdata_in: in std_logic_vector(31 downto 0);
        txcharisk_in: in std_logic_vector(3 downto 0);
        TTC_o : out TTC_data_type;
        clk40_ttc_o : out std_logic;
        clk40_ttc_ready_o : out std_logic
    );
end ltittc_decoder_userdesign;

architecture rtl of ltittc_decoder_userdesign is
component gtwizard_0 
    port
    (
        SYSCLK_IN                               : in   std_logic;
        SOFT_RESET_TX_IN                        : in   std_logic;
        SOFT_RESET_RX_IN                        : in   std_logic;
        DONT_RESET_ON_DATA_ERROR_IN             : in   std_logic;
        GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
        GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
        GT0_DATA_VALID_IN                       : in   std_logic;
    
        --_________________________________________________________________________
        --GT0  (X0Y4)
        --____________________________CHANNEL PORTS________________________________
        --------------------------------- CPLL Ports -------------------------------
        gt0_cpllfbclklost_out                   : out  std_logic;
        gt0_cplllock_out                        : out  std_logic;
        gt0_cplllockdetclk_in                   : in   std_logic;
        gt0_cpllreset_in                        : in   std_logic;
        -------------------------- Channel - Clocking Ports ------------------------
        gt0_gtrefclk0_in                        : in   std_logic;
        gt0_gtrefclk1_in                        : in   std_logic;
        ---------------------------- Channel - DRP Ports  --------------------------
        gt0_drpaddr_in                          : in   std_logic_vector(8 downto 0);
        gt0_drpclk_in                           : in   std_logic;
        gt0_drpdi_in                            : in   std_logic_vector(15 downto 0);
        gt0_drpdo_out                           : out  std_logic_vector(15 downto 0);
        gt0_drpen_in                            : in   std_logic;
        gt0_drprdy_out                          : out  std_logic;
        gt0_drpwe_in                            : in   std_logic;
        --------------------- RX Initialization and Reset Ports --------------------
        gt0_eyescanreset_in                     : in   std_logic;
        gt0_rxuserrdy_in                        : in   std_logic;
        -------------------------- RX Margin Analysis Ports ------------------------
        gt0_eyescandataerror_out                : out  std_logic;
        gt0_eyescantrigger_in                   : in   std_logic;
        --------------- Receive Ports - Comma Detection and Alignment --------------
        gt0_rxslide_in                          : in   std_logic;
        ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt0_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt0_rxusrclk_in                         : in   std_logic;
        gt0_rxusrclk2_in                        : in   std_logic;
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt0_rxdata_out                          : out  std_logic_vector(39 downto 0);
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt0_gthrxn_in                           : in   std_logic;
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt0_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
        gt0_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt0_rxoutclkfabric_out                  : out  std_logic;
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt0_gtrxreset_in                        : in   std_logic;
        ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt0_gthrxp_in                           : in   std_logic;
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt0_rxresetdone_out                     : out  std_logic;
        --------------------- TX Initialization and Reset Ports --------------------
        gt0_gttxreset_in                        : in   std_logic;
        gt0_txuserrdy_in                        : in   std_logic;
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        gt0_txusrclk_in                         : in   std_logic;
        gt0_txusrclk2_in                        : in   std_logic;
        ------------------ Transmit Ports - TX Data Path interface -----------------
        gt0_txdata_in                           : in   std_logic_vector(31 downto 0);
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt0_gthtxn_out                          : out  std_logic;
        gt0_gthtxp_out                          : out  std_logic;
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt0_txoutclk_out                        : out  std_logic;
        gt0_txoutclkfabric_out                  : out  std_logic;
        gt0_txoutclkpcs_out                     : out  std_logic;
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt0_txresetdone_out                     : out  std_logic;
        ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
        gt0_txcharisk_in                        : in   std_logic_vector(3 downto 0);
    
    
        --____________________________COMMON PORTS________________________________
         GT0_QPLLOUTCLK_IN  : in std_logic;
         GT0_QPLLOUTREFCLK_IN : in std_logic
    
    );
    end component;
    
    signal gt0_rxslide_in: std_logic := '0';
    
    signal gt0_rxusrclk_in, gt0_rxusrclk2_in: std_logic;
    signal gt0_rxdata_out: std_logic_vector(39 downto 0);
    signal gt0_gthrxn_in, gt0_gthrxp_in, gt0_gthtxn_out, gt0_gthtxp_out: std_logic;
    signal gt0_txusrclk_in, gt0_txusrclk2_in, gt0_txoutclk_out: std_logic;
    
    
    
    signal rxdata_s: std_logic_vector(31 downto 0);
    signal rxk_s: std_logic_vector(3 downto 0);
    signal rx_code_err_s: std_logic_vector(3 downto 0); -- @suppress "signal rx_code_err_s is never read"
    signal rx_disp_err_s: std_logic_vector(3 downto 0); -- @suppress "signal rx_disp_err_s is never read"
    signal decoder_aligned_s: std_logic;

begin

    
    txusrclk_bufg: BUFG port map(
        O => gt0_txusrclk_in,
        I => gt0_txoutclk_out
    );

    txusrclk2_bufg: BUFG port map(
        O => gt0_txusrclk2_in,
        I => gt0_txoutclk_out
    );
        
    gt0_rxusrclk_in <= gt0_txusrclk_in;
    gt0_rxusrclk2_in <= gt0_txusrclk2_in;
    
    gt0_gthrxn_in <= gt0_gthtxn_out;
    gt0_gthrxp_in <= gt0_gthtxp_out;
    
    gtwizard_0_i : gtwizard_0
    port map
    (
        SYSCLK_IN                       =>      clk60_i,
        SOFT_RESET_TX_IN                =>      reset_i,
        SOFT_RESET_RX_IN                =>      reset_i,
        DONT_RESET_ON_DATA_ERROR_IN     =>      '1',
        GT0_TX_FSM_RESET_DONE_OUT       => open,
        GT0_RX_FSM_RESET_DONE_OUT       => open,
        GT0_DATA_VALID_IN => '1',

    --_________________________________________________________________________
    --GT0  (X0Y4)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
        gt0_cpllfbclklost_out           =>      open,
        gt0_cplllock_out                =>      open,
        gt0_cplllockdetclk_in           =>      clk60_i,
        gt0_cpllreset_in                =>      reset_i,
    -------------------------- Channel - Clocking Ports ------------------------
        gt0_gtrefclk0_in                =>      clk240_i,
        gt0_gtrefclk1_in                =>      clk240_i,
    ---------------------------- Channel - DRP Ports  --------------------------
        gt0_drpaddr_in                  =>      (others => '0'),
        gt0_drpclk_in                   =>      clk60_i,
        gt0_drpdi_in                    =>      (others => '0'),
        gt0_drpdo_out                   =>      open,
        gt0_drpen_in                    =>      '0',
        gt0_drprdy_out                  =>      open,
        gt0_drpwe_in                    =>      '0',
    --------------------- RX Initialization and Reset Ports --------------------
        gt0_eyescanreset_in             =>      '0',
        gt0_rxuserrdy_in                =>      '1',
    -------------------------- RX Margin Analysis Ports ------------------------
        gt0_eyescandataerror_out        =>      open,
        gt0_eyescantrigger_in           =>      '0',
    --------------- Receive Ports - Comma Detection and Alignment --------------
        gt0_rxslide_in                  =>      gt0_rxslide_in,
    ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt0_dmonitorout_out             =>      open,
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt0_rxusrclk_in                 =>      gt0_rxusrclk_in,
        gt0_rxusrclk2_in                =>      gt0_rxusrclk2_in,
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt0_rxdata_out                  =>      gt0_rxdata_out,
    ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt0_gthrxn_in                   =>      gt0_gthrxn_in,
    --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt0_rxmonitorout_out            =>      open,
        gt0_rxmonitorsel_in             =>      "00",
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt0_rxoutclkfabric_out          =>      open,
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt0_gtrxreset_in                =>      reset_i,
    ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt0_gthrxp_in                   =>      gt0_gthrxp_in,
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt0_rxresetdone_out             =>      open,
    --------------------- TX Initialization and Reset Ports --------------------
        gt0_gttxreset_in                =>      reset_i,
        gt0_txuserrdy_in                =>      '1',
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        gt0_txusrclk_in                 =>      gt0_txusrclk_in,
        gt0_txusrclk2_in                =>      gt0_txusrclk2_in,
    ------------------ Transmit Ports - TX Data Path interface -----------------
        gt0_txdata_in                   =>      txdata_in,
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt0_gthtxn_out                  =>      gt0_gthtxn_out,
        gt0_gthtxp_out                  =>      gt0_gthtxp_out,
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt0_txoutclk_out                =>      gt0_txoutclk_out,
        gt0_txoutclkfabric_out          =>      open,
        gt0_txoutclkpcs_out             =>      open,
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt0_txresetdone_out             =>      open,
    ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
        gt0_txcharisk_in                =>      txcharisk_in,


    --____________________________COMMON PORTS________________________________
         GT0_QPLLOUTCLK_IN  => '0',
         GT0_QPLLOUTREFCLK_IN => '0' 
    
    );
    
    --!Decodes 8b10b from 40b raw data, provides K28.5 alginment and asserts rxslide if not aligned.
    dec0:  entity work.dec_8b10b_4x 
    Port map(
        reset_i => reset_i,
        clk_i => gt0_rxusrclk2_in,
        data_i => gt0_rxdata_out,
        data_o => rxdata_s,
        k_o => rxk_s,
        code_err_o => rx_code_err_s,
        disp_err_o => rx_disp_err_s,
        rxslide_o => gt0_rxslide_in,
        decoder_aligned_o => decoder_aligned_s
    );
    
    lti0: entity work.ltittc_wrapper port map(
        reset_in => reset_i,
        clk240_in => gt0_rxusrclk2_in,
        linkaligned_in => decoder_aligned_s,
        rxdata_in => rxdata_s,
        rxcharisk_in => rxk_s,
        TTC_out => TTC_o,
        clk40_ttc_out => clk40_ttc_o,
        clk40_ttc_ready_out => clk40_ttc_ready_o
    );

end architecture rtl;

