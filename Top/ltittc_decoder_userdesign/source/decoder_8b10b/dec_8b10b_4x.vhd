--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--! Takes 8b10b encoded (40-bit, 4 bytes) data and decodes into 4 bytes + charisk.
--! This entity also takes care of bit alignment (rxslide_o) of the transceiver
--! It is assumed that the K28.5 character occurs once every 6 clock cycles (TTC-LTI format).

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;



entity dec_8b10b_4x is
  Port (
    reset_i: in std_logic;                          --! Active high reset
    clk_i : in std_logic;                           --! 240.474 MHz rxoutclk from transceiver
    data_i : in std_logic_vector(39 downto 0);      --! raw 8b10b encoded data from transceiver
    data_o : out std_logic_vector(31 downto 0);     --! decoded data output
    k_o : out std_logic_vector(3 downto 0);         --! decoded CharIsK output
    code_err_o : out std_logic_vector(3 downto 0);  --! 8b10b coding error (1 per byte)
    disp_err_o : out std_logic_vector(3 downto 0);  --! 8b10b disparity error (1 per byte)
    rxslide_o : out std_logic;                      --! bitslip output, connect to transceiver
    decoder_aligned_o : out std_logic               --! indication that K28.5 is found
  );
end dec_8b10b_4x;

architecture Behavioral of dec_8b10b_4x is

    signal dispin_s, dispout_s: std_logic_vector(3 downto 0);
    --! bit3 of dispout should be pipelined to the next clock cycle
    --! All other bits' parity are calculated in the same clock
    signal dispout3_p1_s: std_logic;
    
    signal data_s: std_logic_vector(31 downto 0);
    signal k_s: std_logic_vector(3 downto 0);
    
begin
    data_o <= data_s;
    k_o <= k_s;
    g4: for i in 0 to 3 generate
            dec0: entity work.dec_8b10b port map(
                reset => reset_i,
                clk => clk_i,
                datain => data_i(i*10+9 downto i*10),
                datain_valid => '1',
                ko => k_s(i),
                dataout => data_s(i*8+7 downto i*8),
                code_err => code_err_o(i),
                disp_err => disp_err_o(i),
                dispin => dispin_s(i),
                dispout => dispout_s(i)
            );
    end generate;
    
    process(clk_i)
    begin
        if rising_edge(clk_i) then
            dispout3_p1_s <= dispout_s(3); --Only register bit3, other bits parity calculated in same clock
        end if;
    end process;
    
    dispin_s(0) <= dispout3_p1_s;
    dispin_s(1) <= dispout_s(0);
    dispin_s(2) <= dispout_s(1);
    dispin_s(3) <= dispout_s(2);
    
  --! The LTI format should contain a K28.5 (K + 0xBC) in every 6 clock cycles. If not found, bitslip the receiver.
  algign_proc: process(clk_i)
    variable cnt: integer range 0 to 31; --! K28.5 should be available every 6 clock cycles, let's count to 31 instead so we get at least 5 opportunities to align
  begin
    if rising_edge(clk_i) then
        if reset_i = '1' then
            decoder_aligned_o <= '0';
            rxslide_o <= '0';
            cnt := 0;
        else
            rxslide_o <= '0';
            if cnt = 31 then --! No K28.5 found in 15 clocks, bitslip and deassert alignment.
                cnt := 0;
                rxslide_o <= '1';
                decoder_aligned_o <= '1';
            else
                cnt := cnt + 1;
            end if;
            if data_s(7 downto 0) = x"BC" and k_s(0) = '1' then
                cnt := 0;
                decoder_aligned_o <= '1';
            end if;
        end if;
    end if;
  end process;
    
end Behavioral;
