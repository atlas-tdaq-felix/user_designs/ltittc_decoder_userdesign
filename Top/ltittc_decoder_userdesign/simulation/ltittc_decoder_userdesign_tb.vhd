--! This file is part of the FELIX firmware d000000istribution (https://gtlab.cern.ch/atlas-tdaq-felix/firmwtoare/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--! Toplevel simulation testbench:
--!   * ttc_gen_proc Generates L0A, L0ID, TS and BCID.
--!   * LTItx0:TTC_LTI_Transmitter Encodes the TTC record into the 32b + 4b charisK in the LTI data format
--!   * userdesign0:ltittc_decoder_userdesign Contains a simulation model of a Xilinx 7 series GTH transceiver with 8b10b TX and raw 40b RX, a 4-byte 8b10b decoder in VHDL and the ltittc wrapper / decoder.
--!   * check_proc does some basic checking on the decoded TTC signal.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;
use ieee.numeric_std_unsigned.all;


library UNISIM;
use UNISIM.VComponents.all;

use work.lti_types_package.all;

library vunit_lib;
context vunit_lib.vunit_context;


entity ltittc_decoder_userdesign_tb is
    generic (runner_cfg : string);
end ltittc_decoder_userdesign_tb;

architecture Behavioral of ltittc_decoder_userdesign_tb is

    signal clk40, clk60, clk240, reset: std_logic;
    constant clk40_period: time := 24.948 ns;
    constant clk60_period: time := 16.667 ns;
    constant clk240_period: time := 4.158 ns;
    
    
    signal clk40_ttc_s : std_logic;
    signal clk40_ttc_ready_s : std_logic;
    signal txdata_s : std_logic_vector(31 downto 0);
    signal txcharisk_s : std_logic_vector(3 downto 0);
    signal TTC_in_s : TTC_data_type;
    signal TTC_out_s : TTC_data_type;

begin


    clk40_proc: process
    begin
        clk40 <= '1';
        wait for clk40_period/2;
        clk40 <= '0';
        wait for clk40_period/2;
    end process;
    
    clk60_proc: process
    begin
        clk60 <= '1';
        wait for clk60_period/2;
        clk60 <= '0';
        wait for clk60_period/2;
    end process;
    
    clk240_proc: process
    begin
        clk240 <= '1';
        wait for clk240_period/2;
        clk240 <= '0';
        wait for clk240_period/2;
    end process;
    
    reset_proc: process
    begin
        reset <= '1';
        wait for clk60_period * 10;
        reset <= '0';
        wait;
    end process;
    
    ttc_gen_proc: process(clk40)
        variable cnt40: integer range 0 to 39 := 0;
    begin
        if rising_edge(clk40) then
            if reset = '1' or clk40_ttc_ready_s = '0' then
                cnt40 := 0;
                TTC_in_s <= (
                    MT                  => '0',
                    PT                  => '0',
                    Partition           => "00",
                    BCID                => x"000",
                    SyncUserData        => x"0000",
                    SyncGlobalData      => x"0000",
                    TS                  => '0',
                    ErrorFlags          => x"0",
            
                    -- TTC Message
                    SL0ID               => '0',
                    SOrb                => '0',
                    Sync                => '0',
                    GRst                => '0',
                    L0A                 => '0',
                    L0ID                => (others => '0'),
                    OrbitID             => x"0000_0000",
                    TriggerType         => x"0000",
                    LBID                => x"0000",
            
                    --User message
                    AsyncUserData       => x"0000_0000_0000_0000",
            
                    LTI_decoder_aligned  => '1',
                    LTI_CRC_valid        => '1'
                );
            else
                --Increment BCID every BC clock
                if TTC_in_s.BCID /= std_logic_vector(to_unsigned(3563,12)) then
                    TTC_in_s.BCID <= TTC_in_s.BCID + 1;
                    TTC_in_s.TS <= '0';
                else
                    TTC_in_s.BCID <= x"000";
                    TTC_in_s.TS <= '1'; 
                end if;
                
                --Generate 1 MHz L0A and L1ID
                if cnt40 /= 39 then
                    cnt40 := cnt40 + 1;
                    TTC_in_s.L0A <= '0';
                else
                    cnt40 := 0;
                    TTC_in_s.L0ID <= TTC_in_s.L0ID + 1;
                    TTC_in_s.L0A <= '1';
                end if;
            end if;
        end if;
        
    end process;
    
    LTItx0: entity work.TTC_LTI_Transmitter port map(
        clk40                   => clk40,
        reset                   => reset,
        TTCin                   => TTC_in_s,
        LTI_TX_Data_Transceiver => txdata_s,
        LTI_TX_TX_CharIsK       => txcharisk_s,
        LTI_TXUSRCLK_in         => clk240
    );

    userdesign0: entity work.ltittc_decoder_userdesign port map(
        reset_i => reset,
        clk60_i => clk60,
        clk240_i => clk240,
        txdata_in => txdata_s,
        txcharisk_in => txcharisk_s,
        TTC_o => TTC_out_s,
        clk40_ttc_o => clk40_ttc_s,
        clk40_ttc_ready_o => clk40_ttc_ready_s
    );
   
    check_proc: process(clk40_ttc_s)
        variable BCID: std_logic_vector(11 downto 0) := (others => '0');
        variable L0ID: std_logic_vector(37 downto 0) := (others => '0');
        variable timecount: integer := 0;
    begin
        if rising_edge(clk40_ttc_s) then
            timecount := timecount + 1;
            if clk40_ttc_ready_s = '1' then
                if TTC_out_s.LTI_decoder_aligned = '1' then
                    check_equal(TTC_out_s.LTI_CRC_valid , '1');
                    if TTC_out_s.BCID /= (BCID +1) and TTC_out_s.BCID /= x"000" and BCID = std_logic_vector(to_unsigned(3563,12)) then
                        report "Error: Invalid BCID received" severity ERROR;
                    end if;
                    BCID := TTC_out_s.BCID;                                                  
                    if TTC_out_s.L0ID /= (L0ID +1) and TTC_out_s.L0ID /= L0ID then
                        report "Error: Invalid L0ID received" severity ERROR;
                    end if;
                    L0ID := TTC_out_s.L0ID;
                else
                    report "Warning: LTI_decoder not yet aligned" severity WARNING;
                end if;
           end if;
           
       end if;
    end process;
   
    main : process
    begin
        test_runner_setup(runner, runner_cfg);
        wait for 18 us;
        check_equal(TTC_out_s.LTI_decoder_aligned, '1');
        wait for 100 us;
        
        test_runner_cleanup(runner); -- Simulation ends here
    end process;

end Behavioral;
